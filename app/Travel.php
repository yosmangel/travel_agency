<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
	/**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'travel_code','places','destiny','origin_place','price'
    ];

    /**
     * [traveler relationship]
     * 
     */
    public function traveler(){
    	return $this->belongsToMany(Travelers::class,'travels_travelers','travel_id','traveler_id');
    }
}
