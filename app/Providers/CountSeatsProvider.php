<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
Use DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class CountSeatsProvider extends ServiceProvider
{
    /**
     * [boot Valida si quedan asientos disponibles para el viaje en cuestion]
     * [$parameters[0]] [ID del viaje]
     * @return true cuando quedan puestos disponibles
     * @return false cuando ya no quedan en existencia
     */
    public function boot()
    {
        Validator::extend('count_seat', function($attribute, $value, $parameters, $validator) {
            $travel_id = $parameters[0];

            $travel=DB::table('travels')->where('id',$travel_id)->first();
            if(!$travel){
                return false;
            }
            if(intval($travel->places)==0){
                return false;
            }else{
                return true;
            }
        });   

        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
