<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travelers extends Model
{
	/**
	 * [$fillable description]
	 * @var [parameters]
	 */
    protected $fillable = [
        'cedula','name','direction','phone'
    ];

    /**
     * [travel relationship]
     * 
     */
    public function travel(){
    	return $this->belongsToMany(Travel::class,'travels_travelers','traveler_id','travel_id');
    }
}
