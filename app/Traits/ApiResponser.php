<?php  


namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait para el manejo de las respuestas JSON enviadas desde la aplicacion
 */
trait ApiResponser{

	/**
	 * [successResponse Respuesta exitosa]
	 * @param  [type] $data [Data a procesar]
	 * @param  [type] $code [codigo de la respuesta]
	 * @return [type]       [Respuesta en formato JSON]
	 */
	private function successResponse($data, $code){
		return response()->json($data,$code);
	}
	/**
	 * [errorResponse Respuesta de error]
	 * @param  [type] $message [Mensaje atado a la respuesta JSON]
	 * @param  [type] $code [codigo de la respuesta]
	 * @return [type]       [Respuesta en formato JSON]
	 */
	protected function errorResponse($message, $code){
		return response()->json(['error'=>$message, 'code'=>$code], $code);
	}
	/**
	 * [messageResponse ]
	 * @param  [type] $message [Mensaje atado a la respuesta JSON]
	 * @param  [type] $code [codigo de la respuesta]
	 * @return [type]       [Respuesta en formato JSON]
	 */
	protected function messageResponse($message, $code){
		return response()->json(['message'=>$message, 'code'=>$code], $code);
	}
	/**
	 * [showAll Mostrar Coleccion de datos]
	 * @param  [type] $collection [Array de datos]
	 * @param  [type] $code [codigo de la respuesta]
	 * @return [type]       [Uso de la respuesta exitosa para retornar la coleccion de datos]
	 */
	protected function showAll(Collection $collection, $code=200){
		return $this->successResponse(['data'=>$collection], $code);
	}
	/**
	 * [ showOne Mostrar registro de un modelo]
	 * @param  [type] $instance [Instancia de datos de un modelo]
	 * @param  [type] $code [codigo de la respuesta]
	 * @return [type]       [Uso de la respuesta exitosa para retornar la coleccion de datos]
	 */
	protected function showOne(Model $instance, $code=200){
		return $this->successResponse(['data'=>$instance], $code);
	}
}
