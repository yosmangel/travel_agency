<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TravelerTravelRegistryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = parent::all();

        return [
            "travel_id" => "required|exists:travels,id|count_seat:".$data['travel_id'],
            "cedula" => "required|unique:travelers,cedula,".$data['traveler_id'],
            "name" => "required|max:200",
            "direction" => "required",
            "phone" => "required|numeric"
        ];
    }

     public function messages()
    {
        return [
            'cedula.required' => "El campo Cédula es obligatorio",
            'cedula.unique' => "Ya existe esta cédula en nuestra base de datos",
            'name.required' => "El campo Nombre es obligatorio",
            'name.max' => "El campo Nombre no puede superar los 200 caracteres",
            'direction.required' => "El campo Dirección es obligatorio",
            'phone.required' => "El campo Teléfono es obligatorio",
            'phone.numeric' => "El campo Teléfono debe ser numérico",
            'travel_id.required' => "El campo Código de viaje es obligatorio",
            'travel_id.exists' => "No existe el viaje en nuestra base de datos",
            'travel_id.count_seat' => "No existen cupos disponibles para el viaje"
        ];
    }
}
