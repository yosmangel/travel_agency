<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TravelerRegistryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = parent::all();

        return [
            "cedula" => "required|unique:travelers,cedula,".$data['id'],
            "name" => "required|max:200",
            "direction" => "required",
            "phone" => "required|numeric"
        ];
    }

     public function messages()
    {
        return [
            'cedula.required' => "El campo Cédula es obligatorio",
            'cedula.unique' => "Ya existe esta cédula en nuestra base de datos",
            'name.required' => "El campo Nombre es obligatorio",
            'name.max' => "El campo Nombre no puede superar los 200 caracteres",
            'direction.required' => "El campo Dirección es obligatorio",
            'phone.required' => "El campo Teléfono es obligatorio",
            'phone.numeric' => "El campo Teléfono debe ser numérico"
        ];
    }
}
