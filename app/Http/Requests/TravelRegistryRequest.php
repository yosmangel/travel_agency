<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Input;

class TravelRegistryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = parent::all();

        return [
            "places" => "required|numeric",
            "destiny" => "required",
            "travel_code" => "required|unique:travels,travel_code,".$data['id'],
            "origin_place" => "required",
            "price" => "required|numeric"
        ];
    }

     public function messages()
    {
        return [
            'places.required' => "El campo Asientos es obligatorio",
            'places.numeric' => "El campo Asientos debe ser numérico",
            'travel_code.required' => "El campo Código de viaje es obligatorio",
            'travel_code.unique' => "Ya existe este código de viaje en nuestra base de datos",
            'destiny.required' => "El campo Destino es obligatorio",
            'origin_place.required' => "El campo Lugar de origen es obligatorio",
            'prices.required' => "El campo Precio es obligatorio",
            'prices.numeric' => "El campo Precio debe ser numérico"
        ];
    }
}
