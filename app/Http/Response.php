<?php

namespace App\Http;

use Illuminate\Http\JsonResponse;

public function response(array $errors)
{
    //This will always return JSON object error messages
    return new JsonResponse($errors, 422);
}