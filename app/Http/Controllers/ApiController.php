<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;

/**
 * Metodo controlador de las llamadas del api
 */
class ApiController extends Controller
{
    use ApiResponser;
}
