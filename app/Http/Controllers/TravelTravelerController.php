<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Travel;
use App\Travelers;

class TravelTravelerController extends ApiController
{
	/**
	 * [get Viajes con los viajeros registrados]
	 * @return [array] [Retorna la data de los viajes con todos los viajeros asociados al mismo]
	 */
    public function get(){

    	$travel=Travel::with('traveler')->get();

    	return $this->showAll($travel);
    }
}
