<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class HomeController extends ApiController
{
    /**
     * Metodo inicial del api.
     *
     * @return void
     */
    
    /**
     * [index Retorno inicial del api]
     * @return [text] [Mensaje inicial del api]
     */
    public function index()
    {
         return $this->messageResponse("Revisa la información del Api para mas información",200);

    }
}
