<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Travel;
use App\Travelers;
use App\Http\Requests\TravelerTravelRegistryRequest;

class TravelRegistryController extends ApiController
{
    
    /**
     * [create Metodo de registro de usuarios en los viajes (si no existe en los registros, los crea y/o actualiza los datos)]
     * @param  TravelerTravelRegistryRequest $request [Request validador para el proceso de registro de viajeros]
     * @return [status=200]                   [Registro exitoso del pasajero]
     * @return [status=410]                   [Ya el pasajero se encuentra registrado en el viaje]
     * @return [status=400]                   [Error al intentar realizar el registro del pasajero]
     */
    public function create(TravelerTravelRegistryRequest $request){

    	try {
    		$fields = $request;
    		if (empty($fields['traveler_id'])) {
    			$traveler=Travelers::create([
    				'cedula' => $fields['cedula'],
    				'name'=>$fields['name'],
    				'direction'=>$fields['direction'],
    				'phone'=>$fields['phone'],
    			]);
    		}else{
    			$traveler=Travelers::find($fields['traveler_id']);
    			$traveler->cedula = $fields['cedula'];
    			$traveler->name = $fields['name'];
    			$traveler->direction = $fields['direction'];
    			$traveler->phone = $fields['phone'];
                $traveler->save();
    		}

    		$result=$traveler->travel()->find($fields['travel_id']);

            if($result){
                return $this->messageResponse("Estos datos ya se encuentran registrados para este viaje",410);
            }
            
            $traveler->travel()->attach($fields['travel_id']);
    		$destino=Travel::find($fields['travel_id'])->first();
            $destino->decrement('places', 1);
            return $this->messageResponse("Registro del viajero exitoso con destino a ".$destino->destiny,200);
 		
    	} catch (Exception $e) {
    		return  $this->errorResponse("No se pudo agregar al pasajero a el viaje",400);
    	}
    	
    }


}
