<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Travelers;
use App\Http\Requests\TravelerRegistryRequest;

class TravelerController extends ApiController
{
    /**
     * [get Retorna la informacion de los viajeros]
     * @return [array] [Retorna la informacion de todos los viajeros en le registro]
     */
    public function get(){

    	$traveler=Travelers::all();

    	return $this->showAll($traveler);
    }

    /**
     * [create Metodo para el registro de viajeros]
     * @param  TravelerRegistryRequest $request [Request validador de los datos para los viajeros]
     * @return [status=200]                     [Registro exitoso del pasajero]
     * @return [status=400]                     [Error al intentar realizar el registro]
     */
    public function create(TravelerRegistryRequest $request){

    	try {
    		Travelers::create($request->all());
            return $this->messageResponse("Registro de viajero exitoso",200);
 		
    	} catch (Exception $e) {
    		return  $this->errorResponse("No se puedo realizar el registro del viajero",400);
    	}
    	
    }

    /**
     * [update Metodo para la actualizacion de datos de los viajeros]
     * @param  TravelerRegistryRequest $request [Request validador de los datos para los viajeros]
     * @return [status=200]                     [Actualizacion exitosa del pasajero]
     * @return [status=400]                     [Error al intentar realizar la actualizacion]
     */
    public function update(TravelerRegistryRequest $request,$id){

        try {
            $traveler=Travelers::find($id);
            $traveler->cedula = $request['cedula'];
            $traveler->name = $request['name'];
            $traveler->direction = $request['direction'];
            $traveler->phone = $request['phone'];
            $traveler->save();
            return $this->messageResponse("Registro de viajero actualizado",200);
        
        } catch (Exception $e) {
            return  $this->errorResponse("No se pudo realizar la actualización de los datos",200);
        }
        
    }
}
