<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Travel;
use App\Http\Requests\TravelRegistryRequest;
use App\Http\Controllers\ApiController;

class TravelController extends ApiController
{
    
    /**
     * [get Metodo para obtener la data de los viajes]
     * @return [array] [Retorna la data de todos los viajes sin paginado]
     */
    public function get(){

    	$travel=Travel::all();

    	return $this->showAll($travel);
    }

    /**
     * [create Metodo para crear viajes]
     * @param  TravelRegistryRequest $request [Request para evaluar los parametros de los viajes]
     * @return [status=200]                   [Registro exitoso]
     * @return [status=400]                   [Error al intentar actualizar el registro]
     */
    public function create(TravelRegistryRequest $request){

        try {
		    Travel::create($request->all());
            return $this->messageResponse("Registro de viaje exitoso",200);
    	} catch (Exception $e) {
    		return  $this->errorResponse("No se pudo realizar el registro del viaje",400);

    	}
    	
    }

    /**
     * [update Metodo de edicion de viajes]
     * @param  TravelRegistryRequest $request [Request para evaluar los parametros de los viajes]
     * @param                        $id      [Identificador principal del viaje]
     * @return [status=200]                   [Actualización exitosa]
     * @return [status=400]                   [Error al intentar actualizar el registro]
     */
    public function update(TravelRegistryRequest $request,$id){

        try {
            $travel=Travel::find($id);
            $travel->travel_code=$request['travel_code'];
            $travel->places=$request['places'];
            $travel->destiny=$request['destiny'];
            $travel->origin_place=$request['origin_place'];
            $travel->price=$request['price'];
            $travel->save();
            return $this->messageResponse("Registro de viaje actualizado",200);
        } catch (Exception $e) {
            return  $this->errorResponse("No se pudo realizar la actualización de los datos",400);

        }
        
    }
}
