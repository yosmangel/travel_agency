<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Inicio de la aplicacion
Route::get('/', 'HomeController@index')->name('home');

//TRAVEL URL
Route::get('travel', 'TravelController@get')->name('travels');
Route::post('travel', 'TravelController@create')->name('add_travel');
Route::post('travel/{id}', 'TravelController@update')->name('update_travel');

//TRAVELER URL
Route::get('traveler', 'TravelerController@get')->name('travelers');
Route::post('traveler', 'TravelerController@create')->name('add_traveler');
Route::post('traveler/{id}', 'TravelerController@update')->name('edit_traveler');

//TRAVEL- TRAVELER URL
Route::get('travel_travelers', 'TravelTravelerController@get')->name('travel_travelers');
Route::post('travel_travelers', 'TravelRegistryController@create')->name('add_travel_travelers');


