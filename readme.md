Api de prueba sencilla para una Agencia de viajes con procesos basicos.

Puntos a destacar:

    - Se Manejan las excepciones de errores de manera personalizada, esto ayuda a la mejor comprension de los errores para el usuario.
    - Se comentan las funcionalidades para mayor entendimiento del código
    - Solo se incluyen los métodos para visualizar, editar y agregar.
    - Se validan los agregar y editar mediante Request.
    - Al momento de agregar un viajero a cierto viaje, se usa un provider que nos permite certificar si aun existe disponibilidad.